#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.db.form 3.15.90\n"
"POT-Creation-Date: 2020-11-18 18:28 UTC\n"
"PO-Revision-Date: 2020-11-18 18:27 UTC\n"
"Last-Translator: Benoît Minisini <g4mba5@gmail.com>\n"
"Language: zh\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Data bound controls"
msgstr "数据绑定控件"

#: DataControl.class:112 DataView.class:454
msgid "True"
msgstr "真"

#: DataControl.class:113 DataField.class:19 DataView.class:454
msgid "False"
msgstr "假"

#: DataControl.class:114
msgid "Unknown"
msgstr "未知"

#: DataSource.class:421 DataView.class:740
msgid "Invalid value."
msgstr "无效值。"

#: DataSource.class:444
msgid "You must fill all mandatory fields."
msgstr "你必须填充所有的强制字段。"

#: DataView.class:738
msgid "Unable to save value."
msgstr "不能保存值。"

#: DataView.class:904
msgid "Unable to save record."
msgstr "不能保存记录。"

#: DataView.class:1084
msgid "Unable to delete record."
msgstr "不能删除记录。"

#: FBlobEditor.class:123
msgid "Save blob to file"
msgstr "保存Bolb到文件"

#: FBlobEditor.class:132
msgid "Unable to save file"
msgstr "不能保存文件"

#: FBlobEditor.class:138
msgid "Load blob from file"
msgstr "从文件加载Bolb"

#: FBlobEditor.class:147
msgid "Unable to load file"
msgstr "不能加载文件"

#: FBlobEditor.class:153
msgid "Cancel"
msgstr "取消"

#: FBlobEditor.class:153
msgid "Do you really want to clear blob?"
msgstr "确定要清空Bolb吗？"

#: FBlobEditor.class:161
msgid "Unable to clear blob"
msgstr "不能清空Bolb"

#: FBlobEditor.form:15
msgid "Blob contents"
msgstr "Blob内容"

#: FBlobEditor.form:24 FBrowser.form:50
msgid "Save"
msgstr "保存"

#: FBlobEditor.form:31
msgid "Load"
msgstr "加载"

#: FBlobEditor.form:38
msgid "Clear"
msgstr "清除"

#: FBrowser.form:44
msgid "New"
msgstr "新建"

#: FBrowser.form:56 FMain2.form:41 FMain3.form:20 FTest.form:34
msgid "Refresh"
msgstr "刷新"

#: FBrowser.form:62
msgid "Delete"
msgstr "删除"

#: FBrowser.form:68
msgid "Start"
msgstr "开始"

#: FBrowser.form:74
msgid "Previous"
msgstr "上一个"

#: FBrowser.form:80
msgid "Next"
msgstr "下一个"

#: FBrowser.form:86
msgid "End"
msgstr "结束"

#: FMain2.form:90
msgid "Id"
msgstr ""

#: FMain2.form:95
msgid "Color"
msgstr ""

#: FMain2.form:100
msgid "First Name"
msgstr ""

#: FMain2.form:105
msgid "Name"
msgstr ""

#: FMain2.form:110
msgid "Birth"
msgstr ""

#: FMain2.form:115
msgid "Active"
msgstr ""

#: FMain2.form:121
msgid "Comment"
msgstr ""

#: FMain2.form:127
msgid "Salary"
msgstr ""

#: FMain2.form:132
msgid "Image"
msgstr ""

#: FMain3.form:25
msgid "Info"
msgstr ""

#: FTest.form:29
msgid "Create"
msgstr ""

#: FTest.form:39
msgid "Élément 1"
msgstr ""

#: FTest.form:39
msgid "Élément 2"
msgstr ""

#: FTest.form:39
msgid "Élément 3"
msgstr ""

#: FTest.form:39
msgid "Élément 4"
msgstr ""

#: FTest.form:39
msgid "Élément 5"
msgstr ""

#: FTest.form:39
msgid "Élément 6"
msgstr ""

#: FTest.form:39
msgid "Élément 7"
msgstr ""

#: FTest.form:44
msgid "Editable"
msgstr ""

#: FTest.form:49
msgid "Resize"
msgstr ""

#: FTest.form:54
msgid "Menu"
msgstr ""

#: FTest.form:59
msgid "Maximized"
msgstr ""
